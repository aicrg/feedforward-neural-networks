# README #

This README is the document which states whatever steps are necessary to get the application up and running.

### What is this repository for? ###

* This repository is for demonstrating the training and running of the Feed Forward Neural Network
* The Neural network used in this repository uses only three layers; input, output, and one hidden layer.
* We have used the wine data set to train and test the network 
* The g++ compiler was used over Linux(Ubuntu 14.04) to compile this project

### How do I get set up? ###

* Download all the files in the repository and put them together in one folder.
* Open the terminal and change the directory until you locate your folder and go to to that directory.
* Compile the code using the code; " g++ neural_net_code.cpp "
* Run the program ; " ./a.out "
      ***Note that you are using Linux operating system and g++ compiler ***


### Contact Us###

* if you need further clarification on the code then you could contact us on the following emails;
* c.rohitash@gmail.com --> Rohitash Chandra
* deo.ratneel@gmail.com --> Ratneel Deo